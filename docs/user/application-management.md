# Application management

a.k.a "the puzzle client reference".

## Initializing and creating an application

It all starts with a git repository containining your application source code.
You need an account on a puzzle server, let's say `puzzle.example.com`.

The first step is to initialize puzzle on your repository:

```console
$ puzzle init --server=puzzle.example.com
```

This will add a special remote called `puzzle` to your repository:

```console
$ git remote -v
puzzle	puzzle@puzzle.example.com:myapplication (fetch)
puzzle	puzzle@puzzle.example.com:myapplication (push)
```

Note that `myapplication`. puzzle will by default use the same name as your git
repository. You can use any name, and call tell `puzzle init` that via the
`--app` option:

```console
puzzle init --server=puzzle.example.com --app=myapp-production`
```

With the puzzle remote configured, you can now create the application on the
server side:

```console
puzzle create
```

This will create the application structure on the server side, but since we
didn't push any code, there will be nothing there. It' useful if you need to do
further configuration (e.g. setting environment variables etc) even before
doing the first deploy. For example some applications might require a few
environment variables set before they can even start at all.

Command | Description
--------|------------
puzzle init | Create the puzzle remote on your git repository
puzzle create | Creates the application on the server side.

## Deploying

### push deployment

This is the simplest way to deploy a puzzle application, and doesn't even
require the puzzle client. Just `git push` to the puzzle remote, and your
application will be deployed.

```console
git push puzzle
```

### pull deployment

TODO

```console
git push puzzle
```

## puzzle-specific configuration

puzzle supports a few configuration variables to its operation. To manipulate
these configurations, you can use the following commands:

Command | Description
--------|------------
puzzle config set CONFIG VALUE | Sets CONFIG to VALUE.
puzzle config get CONFIG | Prints the value of CONFIG.
puzzle config unset CONFIG | Unsets CONFIG (i.e. resets it to the default setting).
puzzle list | Lists all configurationn variables that are set.

The following configuration variables are recognized:

Configuration variable | Funcionality | Default
-----------------------|--------------|--------
`altroot` | Root of the application source | Root of the git repository
`data-dir` | Mount point inside the container for persistent data directory | /data
`port` | Port inside the container where the application is expected to listen. It will be passed to the container at runtime as the `$PORT` environment variable. | 5000
`pull-repository` | git URL to pull from. See [pull deployment](#pull-deployment). | None
`pull-branch` | git branch to pull from. See [pull deployment](#pull-deployment) | None
`startup-timeout` | Number of seconds to wait for the application to start. If the application is not listening to the expected port after this many seconds, the deploy is considered as having failed. | `60`

## Managing environment variables

Environment variables can be managed using the following commands:

Command | Description
--------|------------
puzzle showenv | Shows all the defined environment variables.
puzzle setenv VAR VALUE | Sets the variable `VAR` to `VALUE`.
puzzle delenv VAR | Deletes the variable `VAR`.

Here is an example of how these commands can be used:

```console
$ puzzle showenv
$ puzzle setenv FOO BAR
$ puzzle showenv
FOO=BAR
$ puzzle delenv FOO
$ puzzle showenv
$
```

Note that in order for the application to have access to any added or modifieed
environment variable, it *needs to be restarted*.

## Managing domain names

puzzle allows you to easily associate custom domain names to applications.
Note, however, that setting up DNS entries is outside of the puzzle scope. Any
domains need to be previously correctly configured in DNS to point to your
puzzle server.

When adding a domain that has a public IP address, puzzle will automatically
obtain a Let's Encrypt SSL certificate for it, and configure the frontend
web server to redirect all HTTP requests to HTTPS.

Command | Description
--------|------------
puzzle cname list | Lists domains currently associated with the appliacation.
puzzle cname add DOMAIN | Adds a new domain to this application.
puzzle cname rm DOMAIN | Removes a domain from this application.
puzzle cname fulllist | Lists all associated domains, including automatically added ones.

## Logs

`puzzle log` will display the application logs, i.e. any output the application
produces on the console (both `stdout` and `stderr`).

Command | Description
--------|------------
puzzle log | Displays the logs from the application

## Running commands in the application environment

`puzzle exec` will allow you to run an one-off command on the application
environment. This environment is a clean container using the same image as the
application (i.e. the application code is available), has access to the
application data directory and the same environment variables as the
application itself, but it's *not the same container* that is running the
application itself. For example if your application has extra logs that are not
sent to the console, you can inspect them with:

```console
puzzle exec cat /var/path/to/app.log
```

If, on the other hand, you an interactive shell, you can use `puzzle shell`. As
with `puzzle exec`, your interactive shell will be in the same environment as
the application, but not in the application container itself.

Command | Description
--------|------------
puzzle exec COMMAND [ARGS] | Executes an one-off command on the application environment.
puzzle shell | Executes an interactive shell on the application environment.

## Managing the application lifecycle

If you need to take your application offline, you can use `puzzle stop`. This
will shutdown the application, and requests to it will return a 502 (Bad
Gateway) HTTP error after a timeout. To start it again, run `puzzle start`.

Some configuration changes, such as setting environment variables, require
restarting the application, and this can be achieved with `puzzle restart`.

Command | Description
--------|------------
puzzle stop | Stop the application
puzzle stop | Start the application
puzzle restart | Restarts the application

## Managing services

## Multiple deployments from the same repository

By default, all command will act based on the data that can be obtained from
the `puzzle` git remote. However, that's only the default, and you can use any
other remote. For example, suppose you want to deploy a staging and a
production of your application. For this, you need two separate remotes,
pointing to two different applications on the server side. Example:

```console
$ puzzle init --server=terceiro.xyz --remote=prod --app=myapp-production
$ puzzle init --server=terceiro.xyz --remote=staging --app=myapp-staging
$ git remote -v
prod	puzzle@terceiro.xyz:myapp-production (fetch)
prod	puzzle@terceiro.xyz:myapp-production (push)
staging	puzzle@terceiro.xyz:myapp-staging (fetch)
staging	puzzle@terceiro.xyz:myapp-staging (push)
```

You can then deploy to both instances independently:

```console
$ git push staging
$ git push prod
```

To manage each different deployment, you just need to inform which remote to
act upon using the `-r/--remote` option:

```console
$ puzzle -r staging setenv ENV staging
$ puzzle -r prod setenv ENV production
```

## Information about the puzzle server

puzzle is distributed under the GNU Affero Public License version 3. This means
that if you are using a puzzle server, you have the right to obtain the
corresponding source code. The following commands let you interact with puzzle
to inspect the server.

Command | Description
--------|------------
puzzle version | Shows the version of puzzle running on the server.
puzzle source | Downloads the puzzle source code running on the server.
