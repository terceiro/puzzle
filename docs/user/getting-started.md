# Getting started with puzzle

Before being able to deploy applications to a puzzle server, you must be given
an account by the server administrator.

## Installing the puzzle client

The puzzle client is your entry point to most admistrative tasks you will need
to do on your deployed application. There are a few different ways to install
it.

### Using the Debian package

Install the `puzzle-client` package

### Manually

[Download the script](https://salsa.debian.org/terceiro/puzzle/-/raw/master/bin/puzzle?inline=false)
   put it in your PATH, and make it executable:

```
wget -O /usr/local/bin/puzzle https://salsa.debian.org/terceiro/puzzle/-/raw/master/bin/puzzle
chmod +x /usr/local/bin/puzzle
```

## Deploying a static HTML site

Let's say you have a static HTML site that is already managed in a git
repository. In this example we will assume a very simple site with just
`index.html`.

```
$ git ls-tree -r HEAD
100644 blob acaf54050d00bdaa2fb699261c6d4d2625eed55d	index.html
$ cat index.html
<!doctype html>
<html>
  <body>
    <h1>Hello, world!</h1>
  </body>
</html>
```

The first step is to initialize puzzle in your git repository:

```
$ puzzle init --server=mypuzzle.server
$ git remote -v
puzzle	puzzle@mypuzzle.server:simple (fetch)
puzzle	puzzle@mypuzzle.server:simple (push)
```

The only thing that `puzzle init` does to your repository is adding a new
specially-named remote called `puzzle`. Deploying your site is now just a
matter of doing a git push to it:

```
$ git push puzzle
Initialized empty Git repository in /var/lib/puzzle/apps/simple/
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Delta compression using up to 4 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 316 bytes | 316.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
remote: Building your app ... (this may take some time)
[...]
remote: Successfully built cb69ec04f225
remote: Successfully tagged puzzle/runtime/simple:2020.09.29.08.45.38
remote: Build successfull!
remote: Checking if application is running OK ...
remote: I: succeeded after 0s
remote: I: Container responded, it's probably working OK
remote: I: access your application at http://simple/
To mypuzzle.server:simple
 * [new branch]      master -> master
```

You will notice at the end that the puzzle server says the application is
available at `http://simple/`. Of course, that's not a real internet address.
But you could already access it at this point if you force an HTTP client to
recognize that address as pointing to your puzzle server. For example, if your
puzzle server IP address is 10.10.10.10, this will work:

```
$ curl --resolve simple:80:10.10.10.10 http://simple/
<!doctype html>
<html>
  <body>
    <h1>Hello, world!</h1>
  </body>
</html>
```

To make your application really accessible on the internet, you must now add a
valid domain to it. The first step is getting a proper DNS entry for your site,
pointing to your puzzle server.

With that in place, you can now add the domain to your application. For this
and other administrative tasks, you will use the puzzle client.

```
$ puzzle cname add mywebsite.com
```

You will notice that this will take a few seconds, and if all goes right, it
won't give you any output. Under the hood, puzzle will be getting a TLS
certificate for you from [Let's Encrypt](https://letsencrypt.org/), so your
users have secure access to your website by default. After it finishes, you can
now access your website securily via its public domain:

```
$ curl https://mywebsite.com/
<!doctype html>
<html>
  <body>
    <h1>Hello, world!</h1>
  </body>
</html>
```

This section gave you an example on how to deploy a simple static website with
puzzle. Of course, simple static websites are not really that exciting, so you
can now move on to the next step and
[deploy an actual web application using docker](docker.md).
