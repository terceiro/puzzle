# Deploying an application with docker

## The simplest web application

Following up on [Getting Started](getting-started.md) tutorial, we will now
transform that simple static website into a simple web application, written in
Ruby and using the web framework Sinatra. Let's first create the application
file and called it `app.rb`:

```ruby
require 'sinatra'

get '/' do
  "Hello, world!\n"
end
```

The source code should be self-explanatory even if you don't do Ruby. The
important thing to know is that running this file through the Ruby interpreter
will start a web server on port 4567 by default:

```console
$ ruby app.rb
== Sinatra (v2.0.8.1) has taken the stage on 4567 for development with backup from Thin
Thin web server (v1.7.2 codename Bachmanity)
Maximum connections set to 1024
Listening on localhost:4567, CTRL+C to stop
```

We can now access the app locally:

```console
$ curl http://localhost:4567/
Hello, world!
```

Let's now deploy this app to puzzle, using Docker.

A `Dockerfile` to deploy this application can look like this:

```dockerfile
FROM debian:buster-slim
RUN apt-get update -q && apt-get install -qy ruby ruby-sinatra
WORKDIR /app
COPY . ./
USER nobody
CMD ruby app.rb -o 0.0.0.0 -p ${PORT:4567}
```

After removing the existing `index.html` and commiting the new files, the
repository looks like this:

```console
$ git ls-tree -r HEAD
100644 blob f3e7405d98122ac12d0a8c9291e1a86c07bd8591	Dockerfile
100644 blob 6b03d12640de4d3846c3fa1bdf077535d6ee4d0d	app.rb
```

## Deploying

To deploy, just push to the `puzzle` remote.

```console
simple (master) [128]$ git push puzzle
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 476 bytes | 476.00 KiB/s, done.
Total 4 (delta 0), reused 0 (delta 0), pack-reused 0
remote: Building your app ... (this may take some time)
remote: Sending build context to Docker daemon  3.072kB
remote: Step 1/5 : FROM debian:buster-slim
remote: buster-slim: Pulling from library/debian
remote: Digest: sha256:b527bec3708ee1fe2ffb4625d742a60bb45b06064e7c64a81b16550fe42710f5
remote: Status: Image is up to date for debian:buster-slim
remote:  ---> 052664ad4351
remote: Step 2/5 : RUN apt-get update -q && apt-get install -qy ruby ruby-sinatra
remote:  ---> Running in f83c5b82456f
[...]
remote: Removing intermediate container f83c5b82456f
remote:  ---> 1520b8f39db1
remote: Step 3/5 : WORKDIR /app
remote:  ---> Running in 8fd1b7d6ce9b
remote: Removing intermediate container 8fd1b7d6ce9b
remote:  ---> 0ff33238190e
remote: Step 4/5 : COPY . ./
remote:  ---> 6ec4242854cf
remote: Step 5/5 : CMD ruby app.rb -o 0.0.0.0 -p $PORT
remote:  ---> Running in 1ef3ca2a5ba9
remote: Removing intermediate container 1ef3ca2a5ba9
remote:  ---> 68942100fc04
remote: Successfully built 68942100fc04
remote: Successfully tagged puzzle/runtime/simple:2020.09.29.09.32.34
remote: Build successfull!
remote: Checking if application is running OK ...
remote: I: succeeded after 1s
remote: I: Container responded, it's probably working OK
remote: I: access your application at http://simple/
remote: 7b4229b0bd04
To mypuzzle.server:simple
   8d0fcd0..4ba34c5  master -> master
```

When a deployed application has a `Dockerfile`, puzzle will build the container
from that Dockerfile and use that as is. After the deployment is finished, you
can now hit our application, and get the desired result:

```console
$ curl https://mywebsite.com/
Hello, world!
```

## Requirements for Docker deployment

The only requirement for the Dockerfile, really, is that the application binds
to 0.0.0.0, on port `$PORT`. `$PORT` is a reserved environment variable that
the puzzl engine will set for all applications. puzzle expects apps to listen
to that port, on all external addresses available to the container (thus
0.0.0.0).

## Features available

When your application runs, the following environment variables are pre-set by
puzzle:

- `$APP_DATA_DIR`: a persistent directory where you can write. This directory
  will always be available to your application, cross deploys. Any other
  location in the filesystem should be considered as volatile, and new deploys
  will get a fresh filesystem, except for `$APP_DATA_DIR`.
- `$PORT`: the port number to which your application must bind, on all external
  addresses (0.0.0.0).

You can also set custom environment variables. See
[application management](application-management.md) for instructions.

## Good practices

### Recommendations from The Twelve-Factor App

[The Twelve-Factor App](https://12factor.net/) is a good set of best practices
to following when designing applications. Those principles make it easier to
have applications running in Docker, but also are great recommendations
application development and deployment in general.

### Minimal privileges

Another good practice is to not run applications as root, which is the default
in Docker. While puzzle will not enforce this, having most of the filesystem be
read only for the user under which the application is running is very useful
for security.

Notice how in the example above, the `Dockerfile` explicitly sets application
to run as `nobody`.

### Optimize serving static assets

The puzzle server will act as a reverse proxy for your app, but it won't look
inside it's filesystem. This means that the traditional setup where you have
apache/nginx server static files directly, and only forward actual dynamic
requests to the application won't work in puzzle.

So your application needs to either serve the static assets itself, or use an
external service for those. If you decide to serve the static assets from your
application, make sure you use proven optimization techniques for that, such as
compression, content-based filesnames, proper HTTP caching headers, etc. For
example, Python apps can  implement this by using
[whitenoise](http://whitenoise.evans.io/).
