# Welcome to puzzle

`puzzle` is a Platform as a Service (PaaS) system. It's design to simplify the
process of deploying web applications. It implements this so that deploying
applications is a matter of s single `git push` command.

puzzle is heavily inspired by [Heroku](https://www.heroku.com/), a proprietary
PaaS, and [dokku](https://github.com/dokku/dokku), a free software system.
puzzle is design to take advantage of the [Debian
GNU/Linux](https://www.debian.org/) system, integrate with it, and require no
components outside of the official Debian repository.

## High level architecture

When working with puzzle to deploy an application, developers do not need to
have access to the server, and all operations are performed from a git
repository containing the application source code in their workstation.

![High level architecture](images/high-level-architecture.svg)

The puzzle server might be a single server, or might be a frontend to an entire
cluster; but the developer can ignore the implementation details of the server.
