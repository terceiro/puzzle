puzzlectl() {
  "$PUZZLECTL" "$@"
}

puzzle_requires_root() {
  if [ "$(whoami)" != 'root' ]; then
    echo "E: this command must be run as root."
    exit 1
  fi
}

puzzle_trigger_all() {
  local hookname="$1"
  shift
  for plugin in ${PUZZLE_PLUGINS}; do
    hook="$plugin/hooks/$hookname"
    if [ -x "$hook" ]; then
      "$hook" "$@"
    fi
  done
}

puzzle_get_template() {
  local source="$1"
  local templatename="$2"
  sed -e "1,/^: <<${templatename}\$/ d; /^${templatename}\$/,\$ d" "$source"
}

puzzle_config_frontend() {
  local basedir
  local static
  basedir="$1"
  shift

  # shellcheck disable=SC2068
  for appname in $@; do

    static=${PUZZLE_DATA_DIR}/static/${appname}/current
    backends="$(puzzlectl list-app-backends "$appname")"

    export appname
    export backends

    (
    __upstream__

    if puzzlectl ssl "$appname" present >/dev/null; then
      __virtualhost__ 80
      __server_name__
      __expose_acme_challenges__
      __redirect_to_https__
      __end_virtualhost__

      __virtualhost__ 443
      __server_name__
      __ssl_config__
      __expose_acme_challenges__
      __stats__ "$PUZZLE_DATA_DIR/stats/$appname/output"
      __logging__
      if [ -d "${static}" ]; then
        __root__ "${static}"
      else
        __proxy__ ssl
      fi
      __end_virtualhost__
    else
      __virtualhost__ 80
      __server_name__
      __expose_acme_challenges__
      __stats__ "$PUZZLE_DATA_DIR/stats/$appname/output"
      __logging__
      if [ -d "${static}" ]; then
        __root__ "${static}"
      else
        __proxy__
      fi
      __end_virtualhost__
    fi
    ) > "${basedir}/${appname}.conf"
  done
  shift
}

get_slot() {
  local appname
  appname="${1}"
  puzzlectl config "${appname}" get slot || true
}

get_new_slot() {
  local slot
  slot=$(get_slot "$@")
  if [ -n "$slot" ] && [ "$slot" -eq 0 ]; then
    echo 1
  else
    echo 0
  fi
}

flip_port() {
  local appname
  appname="${1}"
  puzzlectl config "${appname}" set slot "$(get_new_slot "${appname}")"
}

get_port() {
  local appname port slot portfile lockfile
  appname="${1}"
  slot="${2:-}"
  if [ -z "${slot}" ]; then
    slot=$(get_slot "${appname}")
  fi
  if [ -z "${slot}" ]; then
    slot=0
  fi
  portfile="${PUZZLE_DATA_DIR}/ports"
  lockfile="${portfile}.lock"
  port="$(awk '{ if ($2 == "'"${appname}"'") { print($1) } }' "${portfile}" 2>/dev/null || true)"
  if [ -n "${port}" ]; then
    echo $((port + slot))
  else
    # allocate port
    (
      flock --exclusive 9
      if [ -f "${portfile}" ]; then
        port=$(awk '{print($1)}' "${portfile}" 2>/dev/null | sort -n | tail -n 1)
      fi
      if [ -n "${port}" ]; then
        port=$((port + 2))
      else
        port=5000
      fi
      printf "%s\t%s\n" "${port}" "${appname}" >> "${portfile}"
      echo $((port + slot))
    ) 9> "${lockfile}"
  fi
}

get_new_port() {
  local appname
  appname="${1}"
  slot=$(get_new_slot "${appname}")
  get_port "${appname}" "${slot}"
}
