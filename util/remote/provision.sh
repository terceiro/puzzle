#!/bin/sh

set -eu
username="$1"
key="$2"
basedir=$(readlink -f $(dirname $0)/../..)

set -x
apt-get install -qy lsb-release || true


$basedir/util/remote/provision/$(lsb_release -is | tr A-Z a-z)

ln -sf $basedir/util/remote/use-* /usr/local/bin/
ln -sf $basedir/bin/puzzlectl /usr/local/bin/
ln -sf $basedir/util/remote/cleanup.sh /usr/local/bin/puzzle-cleanup

puzzlectl setup
puzzlectl adduser ${username} ${key}

cp -f $basedir/util/remote/host-apt-proxy /usr/local/bin/
cp -f $basedir/util/remote/host-apt-proxy.service /etc/systemd/system/host-apt-proxy.service
systemctl daemon-reload
systemctl enable host-apt-proxy.service
systemctl restart host-apt-proxy.service

