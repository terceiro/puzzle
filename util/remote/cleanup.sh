#!/bin/bash

set -eu

rm -rf /var/lib/puzzle/{apps,data,cache,etc/nginx,etc/apache2,log,static,stats}/testapp*
sed -i -e /testapp/d /var/lib/puzzle/ports
chronic docker system prune -f
docker images --format='{{.Repository}}:{{.Tag}}' | grep puzzle/runtime/testapp | chronic xargs --no-run-if-empty docker rmi
