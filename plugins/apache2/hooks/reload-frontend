#!/bin/sh

set -eu

. "${PUZZLE_LIBDIR}/functions.sh"

if [ ! -x /usr/sbin/apache2 ]; then
  # apache2 not instaled, do nothing
  exit 0
fi

__virtualhost__() {
  local port
  port="$1"
  echo "<VirtualHost *:$port>"
  echo "  RewriteEngine On"
}

__end_virtualhost__() {
  # shellcheck disable=SC2154
  echo "  IncludeOptional ${PUZZLE_DATA_DIR}/etc/apache2-include/${appname}.conf"
  echo "</VirtualHost>"
}

__upstream__() {
  # shellcheck disable=SC2154
  if [ -n "$backends" ]; then
    # shellcheck disable=SC2154
    echo "<Proxy balancer://puzzle_$appname>"
    for backend in $backends; do
      echo "  BalancerMember ${backend}"
    done
    echo "  Order Allow,Deny"
    echo "  Allow from All"
    echo "</Proxy>"
  fi
}

__server_name__() {
  echo "  ServerName ${appname}"
  for cname in $(puzzlectl cname "$appname" fulllist | grep -v "^$appname\$" || true); do
    echo "  ServerAlias ${cname}"
  done
}

__expose_acme_challenges__() {
  echo "  RewriteRule ^/.well-known/acme-challenge/(.*) /var/lib/dehydrated/acme-challenges/\$1"
  echo "  <Directory /var/lib/dehydrated/acme-challenges>"
  echo "    Require all granted"
  echo "  </Directory>"
}

__stats__() {
  echo "  RewriteRule ^/.well-known/stats(.*) $1/\$1 [QSA,L]"
  echo "  <Directory $1>"
  echo "    Require all granted"
  echo "  </Directory>"
}

__redirect_to_https__() {
  echo "  DocumentRoot ${PUZZLE_DATA_DIR}/www/"
  echo "  Redirect permanent / https://%{HTTP_HOST}/"
}

__ssl_config__() {
  local cert chain privkey
  cert="$(puzzlectl ssl "$appname" cert)"
  privkey="$(puzzlectl ssl "$appname" privkey)"
  chain="$(puzzlectl ssl "$appname" chain)"
  echo '  Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains;"'
  echo "  SSLEngine on"
  echo "  SSLCertificateFile ${cert}"
  echo "  SSLCertificateKeyFile ${privkey}"
  echo "  SSLCertificateChainFile ${chain}"
  echo "  SSLCipherSuite ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA256"
  echo "  RequestHeader set X-Forwarded-SSL \"on\""
  echo "  RequestHeader set X-Forwarded-Proto \"https\""
}

__logging__() {
  echo "  ErrorLog $PUZZLE_DATA_DIR/log/${appname}.error.log"
  echo "  LogLevel warn"
  echo "  CustomLog $PUZZLE_DATA_DIR/log/${appname}.access.log combined"
}

__pull__() {
  echo "  RequestHeader set X-Puzzle-App \"${appname}\""
  echo "  RewriteRule ^/.well-known/pull http://localhost:${PUZZLE_PULL_DAEMON_PORT:-7713}/\$1 [QSA,L]"
}

__root__() {
  local root
  root="${1}"
  __pull__
  echo "  DocumentRoot ${root}/"
  echo "  <Directory ${root}>"
  echo "    Require all granted"
  echo "  </Directory>"
}

__proxy__() {
  __pull__
  if [ -n "$backends" ]; then
    echo "  RewriteRule ^.*$ balancer://puzzle_${appname}%{REQUEST_URI} [P,QSA,L]"
  else
    echo "  DocumentRoot ${PUZZLE_DATA_DIR}/www/"
    echo "  <Directory ${PUZZLE_DATA_DIR}/www>"
    echo "    Require all granted"
    echo "  </Directory>"
  fi
}

puzzle_config_frontend "${PUZZLE_DATA_DIR}/etc/apache2" "$@"
sudo --non-interactive service apache2 reload
