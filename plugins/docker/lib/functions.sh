. "${PUZZLE_LIBDIR}/functions.sh"

puzzle_docker_list_containers() {
  local appname="${1}"
  local tag="${2:-}"

  local image
  if [ -n "$tag" ]; then
    # escape /'s in image name
    image="$(echo "$tag" | sed -e 's/\//\\\//g')"
  else
    image='puzzle\/runtime\/'"${appname}:"
  fi

  docker ps | awk "{ if (\$2 ~ /^${image}/) { print(\$1) } }"
}

puzzle_docker_get_latest_tag() {
  local appname="$1"

  docker images \
    | awk "{ if (\$1 == \"puzzle/runtime/$appname\" && \$2 != \"latest\") { print(\$1 \":\" \$2) } }" \
    | sort | tail -1
}

puzzle_docker_get_container_ip() {
  docker inspect --format='{{.NetworkSettings.IPAddress}}' "$@"
}

get_container_port() {
  local appname="${1}"
  puzzlectl config "$appname" get port || echo 5000
}

puzzle_docker_run() {
  local image="$1"
  local port="$2"
  shift 2

  local docker_options=''
  if [ $# -gt 0 ]; then
    while true; do
      case "$1" in
        -*)
          docker_options="$docker_options $1"
          shift
          ;;
        *)
          break
          ;;
      esac
    done
  fi

  local PORT
  PORT=$(get_container_port "${appname}")

  # starting a application in the background
  run_options="--detach=true --restart=unless-stopped --publish=127.0.0.1:${port}:${PORT}"
  if [ $# -gt 0 ]; then
    # running an interactive command (e.g. puzzle exec)
    run_options='--interactive --rm'
  fi

  local APP_DATA_DIR
  APP_DATA_DIR="$(puzzlectl config "$appname" get data-dir || true)"
  if [ -z "${APP_DATA_DIR}" ]; then
    APP_DATA_DIR=/data
  fi

  local envfile
  envfile="$(mktemp)"

  # environment variables set by enabled services
  puzzlectl service "$appname" showenv >> "$envfile"

  # environment variables set by the application owner
  puzzlectl showenv "$appname" >> "$envfile"

  local data="$PUZZLE_DATA_DIR/data/$appname"

  rc=0
  # shellcheck disable=SC2086
  docker run \
    $run_options \
    $docker_options \
    --env-file="$envfile" \
    --env=PORT="${PORT}" \
    --volume="$data":"$APP_DATA_DIR" \
    --env=APP_DATA_DIR="${APP_DATA_DIR}" \
    "${image}" "$@" || rc=$?

  rm -f "$envfile"
  return $rc
}
