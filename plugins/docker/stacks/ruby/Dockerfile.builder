FROM debian:bookworm
ENV DEBIAN_FRONTEND=noninteractive
RUN \
	apt-get update && \
	apt-get install -qy auto-apt-proxy && \
	apt-get install -qy ruby-dev bundler
COPY build /
