FROM debian:bookworm
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
	&& apt-get install -qy auto-apt-proxy \
	&& apt-get install -qy --no-install-recommends inkscape \
	&& apt-get install -qy sox \
	&& apt-mark hold xmlto texlive\* \
	&& apt-get install -qy nanoc ruby-gettext
COPY build /
