export PUZZLE_CONFIG_PREFIX=puzzle.serviceconfig.postgresql

PUZZLE_POSTGRESQL_PREFIX=

if [ -z "${PUZZLE_POSTGRESQL_SERVER:-}" ]; then

  if ! id postgres >/dev/null 2>&1; then
    echo "E: PostgreSQL is not installed locally."
    echo "E: Also, a remote PostgreSQL server is not configured."
    exit 1
  fi

  # Assume PostgreSQL is running locally and figure out some sane defaults
  # Otherwise all of the variables below must be provided in configuration, and
  # the remote server must be previously configured

  # FIXME: these addresses are docker-specific, and also specific to the
  # default docker configuration.
  PUZZLE_POSTGRESQL_SERVER=$(ifdata -pa docker0)
  # shellcheck disable=SC2034
  PUZZLE_POSTGRESQL_CLIENT_NET=$(docker network inspect --format '{{(index .IPAM.Config 0).Subnet}}' bridge)

  PUZZLE_POSTGRESQL_PREFIX='sudo --non-interactive -u postgres'
fi


puzzle_postgresql_init() {
  local appname="$1"

  puzzle_postgresql_database="puzzle_$appname"
  puzzle_postgresql_username="puzzle_$appname"
}

puzzle_postgresql_user_exists() {
  $PUZZLE_POSTGRESQL_PREFIX psql --quiet --tuples-only \
    -c "select count(*) from pg_user where usename = '$puzzle_postgresql_username';" \
    | grep -q 1
}

puzzle_postgresql_db_exists() {
  $PUZZLE_POSTGRESQL_PREFIX psql --quiet --tuples-only \
    -c "select count(1) from pg_database where datname = '$puzzle_postgresql_database';" \
    | grep -q 1
}
