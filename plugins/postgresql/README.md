# PostgreSQL service for puzzle

## User interface

When the `postgresql` service is added to an application, the `DATABASE_URL`
environment variable will contain the connection information for the
application database. Some frameworks such as Rails will use `DATABASE_URL`
automatically. `PGHOST`, `PGDATABASE`, `PGUSER`, and `PGPASSWORD` environment
variables are also available, and should be used automatically by PostgreSQL
client libraries.

## Setup on local server

If puzzle is running on a single server, and the PostgreSQL server is running
on the same machine, there is no setup necessary and everything will work out
of the box.

## Setup on remote server

For a high-performance/scalable setup, you will want a dedicated PostgreSQL
server. To do this, you must configure the following environment variables:

* `PUZZLE_POSTGRESQL_SERVER`: address (or a resolvable hostname) of the
  PostgreSQL server host.
* `PGUSER`: username used to connect to the PostgreSQL server. The
  corresponding acount must have administrative privileges on PostgreSQL since
  it needs to create databases and other users.
* `PGPASSWORD`: password for the account. Alternatively, this can be provided
  as a `~/.pgpass` file at the `$HOME` of the puzzle user.
