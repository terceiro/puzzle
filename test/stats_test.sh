#!/bin/sh

. "$(dirname "$0")/helper.sh"


test_stats() {
  run_cmd git push puzzle master
  assertHit "$appname"

  # simulate a log rotation; assumes we can login and have root on $SERVER
  ssh_to_server sudo cp /var/lib/puzzle/log/"$appname".access.log{,.1}

  run_cmd ssh puzzle@"$SERVER" update-stats

  assertHit "$appname" "statistics" /.well-known/stats/
  assertHit "$appname" "visitors" /.well-known/stats/data.json
  assertHit "$appname" "7zXZ" /.well-known/stats/archive.tar.xz
}

. shunit2
