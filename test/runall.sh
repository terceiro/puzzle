#!/bin/sh

set -eu

failed=0
for t in test/*_test.sh; do
  echo "===> $t <==="
  rc=0
  sh "$t" || rc=$?
  echo
  if [ "$rc" -ne 0 ]; then
    failed=$((failed + 1))
  fi
done

if [ "$failed" -ne 0 ]; then
  echo "$failed tests failed"
  exit 1
fi
