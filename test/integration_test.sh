#!/bin/sh

. "$(dirname "$0")/helper.sh"

test_version() {
  version=$(ssh puzzle@${SERVER} version 2>/dev/null)
  assertNotEquals "" "$version"
}

test_deploy() {
  run_cmd git push puzzle master
  downloaded_token=$(curl --silent --header "Host: $appname" http://$SERVER/)
  assertHit "$appname" "$token"
}

test_upgrade() {
  run_cmd git push puzzle master
  assertHit "$appname" "$token"
  assertEquals 0 "$?"

  date=$(date)
  echo "$date" > index.html
  git commit -a --quiet -m "$date"
  run_cmd git push puzzle master

  assertHit "$appname" "$date"
}

test_pull() {
  run_cmd git push puzzle master
  run_cmd git pull puzzle master
  assertEquals 0 "$?"
}

. shunit2
