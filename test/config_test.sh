#!/bin/sh

. "$(dirname "$0")/helper.sh"

test_custom_data_dir() {
  run_cmd puzzle config set data-dir /files
  cat >> Dockerfile <<-EOF
FROM busybox
CMD httpd -f -h /files -p \${PORT}
EOF
  run_cmd git add Dockerfile
  run_cmd git commit -m 'Serve /files'
  run_cmd git push puzzle master
  run_cmd puzzle exec -- sh -c 'echo "HELLO WORLD" > /files/index.html'

  assertHit "$appname" "HELLO WORLD"
}

. shunit2
