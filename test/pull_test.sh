#!/bin/sh

. "$(dirname "$0")/helper.sh"

__setup__() {
  run_cmd git push puzzle master
  assertHit "$appname" "$token"

  run_cmd puzzle config set pull-repository "/tmp/${appname}.git"
  echo "HELLO WORLD" > index.html
  run_cmd git commit index.html -m update
  ssh_to_server git init --bare "/tmp/${appname}.git"
  run_cmd git push "$SERVER":"/tmp/${appname}.git" master
}

test_pull() {
  __setup__
  run_cmd puzzle pull
  assertHit "$appname" "HELLO WORLD"
}

test_pull_daemon() {
  if ! ssh_to_server curl --silent --fail 'http://localhost:7713/'; then
    echo "W: pull daemon not running at $SERVER; skipping test"
    return
  fi
  __setup__
  curl --silent -o /dev/null --fail -d '' --header "Host: $appname" "http://$SERVER/.well-known/pull"
  assertHit "$appname" "HELLO WORLD"
}

. shunit2
