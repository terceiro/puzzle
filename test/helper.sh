if test -z "${SERVER:-}"; then
  echo "E: \$SERVER must be set"
  exit 1
fi

if [ -z "$AUTOPKGTEST_TMP" ]; then
  srcdir="$(readlink -f "$(dirname $0)/..")"
  export PATH="${srcdir}/bin:${PATH}"
  tmpdir="$(mktemp --directory --tmpdir puzzle-tests-XXXXXXXXXXX)"
else
  tmpdir="${AUTOPKGTEST_TMP}"
  if [ -z "${DEBUG:-}" ]; then
    trap 'rm -rf ${tmpdir}' INT TERM EXIT
  fi
fi

# use a clean $tmpdir as $HOME
export HOME="$tmpdir"
ssh_config=$HOME/.ssh/config
export GIT_SSH_COMMAND="ssh -F $ssh_config"

# make sure SSH can connect directly to the test server without any prompt
mkdir -p $(dirname $ssh_config)
printf "Host ${SERVER}\n  User root\n  StrictHostKeyChecking no\n  UserKnownHostsFile /dev/null\n" >> $ssh_config

create_dockerfile_for_simple_static_site() {
cat > Dockerfile <<DOCKERFILE
FROM busybox
WORKDIR /app
COPY . ./

USER www-data
CMD ["sh", "-c", "exec httpd -f -p \${PORT:-9999}"]
DOCKERFILE
}

assertHit() {
  local appname="$1"
  local expected_content="$2"
  local path="${3:-/}"

  local waited=0
  while [ "$waited" -le 5 ]; do
    sleep 1
    waited=$(($waited + 1))
    output=$(curl --silent --fail --header "Host: $appname" http://${SERVER}${path})
    if echo "$output" | grep -q "$expected_content"; then
      return
    fi
  done

  fail "Expected content <$expected_content> not found <$output>"
}

assertHeader() {
  local appname="$1"
  local path="${2:-/}"
  local header="$3"
  local value="$4"
  local attempts=1
  local rc
  while [ "$attempts" -le 5 ]; do
    output=$(curl --silent --fail --head --header "Host: $appname" http://${SERVER}${path})
    rc="$?"
    if [ "$rc" -eq 0 ]; then
      break
    fi
    sleep 1
    waited=$(($waited + 1))
  done
  actual_value=$(echo "$output" | sed 's/\r//' | awk "{if(\$1 == \"$header:\") { print(\$2)}}")
  assertContains "$actual_value" "$value"
}

origdir=$(pwd)

setUp() {
  token=$(openssl rand -hex 16)
  appname="testapp${token}"
  setup_app "$appname"
}

change_to_app() {
  local appname="$1"
  cd $tmpdir
  mkdir -p "$appname"
  cd "$appname"
}

run_cmd() {
  if [ -n "${DEBUG:-}" ]; then
    echo '$' "$@"
    "$@"
  else
    chronic "$@"
  fi
}

setup_app() {
  local appname="$1"

  change_to_app "$appname"

  puzzle -s "$SERVER" -a "$appname" init
  git config user.name 'Happy User'
  git config user.email 'happy.user@example.com'

  create_dockerfile_for_simple_static_site
  echo "$token" > index.html
  git add .
  git commit -m "$token" --quiet
  run_cmd puzzle create
}

tearDown() {
  if [ -n "${DEBUG:-}" ]; then
    return
  fi
  run_cmd ssh -F $ssh_config puzzle@${SERVER} stop "$appname"
  cd "$origdir"
}

oneTimeTearDown() {
  if [ -n "${DEBUG:-}" ]; then
    echo "DEBUG: apps left running, and their source left in ${tmpdir} for inspection"
  fi
}

sample_dir="$(readlink -f "$(dirname $0)/sample")"
sample_app() {
  local sample="$1"
  run_cmd git rm -rf *
  cp -r "${sample_dir}/${sample}"/* ./
  run_cmd git add .
  run_cmd git commit -m "sample app: $sample"
}

ssh_to_server() {
  run_cmd ssh -F $ssh_config "$SERVER" -- "$@"
}


if [ -n "$TEST" ]; then
  suite() {
    suite_addTest "$TEST"
  }
fi
