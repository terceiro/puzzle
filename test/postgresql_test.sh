#!/bin/sh

. "$(dirname "$0")/helper.sh"

test_integration() {
  run_cmd puzzle service add postgresql

  cat > Dockerfile <<-EOF
FROM debian
RUN apt-get update && apt-get install -qy postgresql-client busybox
WORKDIR /app
COPY . ./

USER www-data
CMD ["sh", "-c", "exec busybox httpd -f -p \${PORT:-9999}"]
EOF
  run_cmd git add Dockerfile
  run_cmd git commit -m 'PostgreSQL client'
  run_cmd git push puzzle master

  rc=0

  # connection with $DATABASE_URL
  run_cmd puzzle exec -- sh -c 'psql $DATABASE_URL -c "select 1 as foobar"' \
    || rc=$?

  # connection with PG* in the environment
  run_cmd puzzle exec -- sh -c 'psql -c "select 1 as foobar"' \
    || rc=$?
  assertEquals 0 "$rc"
}

. shunit2
