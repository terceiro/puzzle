#!/bin/sh

. "$(dirname "$0")/helper.sh"

test_static_html() {
  run_cmd git rm Dockerfile
  run_cmd git rm -f *
  mkdir public
  echo "HELLO WORLD" > public/index.html
  puzzle config set altroot public
  run_cmd git add public/
  run_cmd git commit -a -m "static site"
  run_cmd git push puzzle master
  assertHit "$appname" "HELLO WORLD"
  assertHeader "$appname" / "Content-Type" "text/html"
}

. shunit2
