#!/bin/sh

. "$(dirname "$0")/helper.sh"

test_integration() {
  run_cmd puzzle service add sqlite3

  cat > Dockerfile <<-EOF
FROM debian
RUN apt-get update && apt-get install -qy ruby-activerecord ruby-sqlite3 busybox
WORKDIR /app
COPY . ./

USER www-data
CMD ["sh", "-c", "exec busybox httpd -f -p \${PORT:-9999}"]
EOF
  run_cmd git add Dockerfile
  run_cmd git commit -m 'SQLite3 user'
  run_cmd git push puzzle master

  rc=0

  # connection with $DATABASE_URL
  run_cmd puzzle exec -- ruby -ractive_record -e 'ActiveRecord::Base.establish_connection; ActiveRecord::Base.connection.execute("SELECT 1")' \
    || rc=$?

  assertEquals 0 "$rc"
}

. shunit2
