#!/bin/sh

. "$(dirname "$0")/helper.sh"

test_run() {
  run_cmd git push puzzle master
  rc=0
  run_cmd puzzle run echo OK || rc=$?
  assertEquals "0" "${rc}"
}

. shunit2
