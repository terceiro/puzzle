#!/bin/sh

. "$(dirname "$0")/helper.sh"

test_env() {
  puzzle setenv FOO bar
  assertTrue 'puzzle showenv | grep FOO=bar'
}

test_no_tty() {
  run_cmd git push puzzle master
  puzzle exec -- sh -c "echo 1 > \$APP_DATA_DIR/date"
  puzzle exec -- sh -c "cat \$APP_DATA_DIR/date" > date.remote
  echo 1 > date.local
  assertTrue "failed to copy data" "cmp date.local date.remote"
}

test_ls_custom_remote() {
  run_cmd git push puzzle master
  run_cmd git remote rename puzzle custom-remote
  assertTrue 'puzzle -r custom-remote ls'
}

test_upload_download() {
  run_cmd git push puzzle master
  echo "DATA" > FILE
  puzzle upload FILE
  rm -f FILE
  puzzle download FILE
  data=$(cat FILE)
  assertEquals "DATA" "$(cat FILE)"
}

. shunit2
