#!/bin/sh

. "$(dirname "$0")/helper.sh"

test_cname() {
  run_cmd git push puzzle master
  run_cmd puzzle cname add example.com
  assertHit "example.com" "$token"
}

test_cname_first_come_first_serve() {
  setup_app "${appname}other"
  run_cmd puzzle cname add used.example.com

  change_to_app "${appname}"
  assertFalse 'cannot add CNAME already in use' 'puzzle cname add used.example.com >/dev/null'
}

test_cname_add_multiple() {
  run_cmd puzzle cname add mult.example.com mult.example.org
  assertTrue "mult.example.com not added" 'puzzle cname list | grep mult.example.com'
  assertTrue "mult.example.org not added" 'puzzle cname list | grep mult.example.org'
}

. shunit2
