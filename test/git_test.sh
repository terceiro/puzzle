. "$(dirname "$0")/helper.sh"

test_main_branch() {
  run_cmd git branch -m main
  run_cmd git push puzzle main
  assertHit "$appname" "$token"
}

. shunit2
