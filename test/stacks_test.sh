#!/bin/sh

. "$(dirname "$0")/helper.sh"

test_static_html() {
  run_cmd git rm Dockerfile
  echo "<foo></foo>" > foo.xml
  run_cmd git add foo.xml
  run_cmd git commit -a -m "static site"
  run_cmd git push puzzle master
  assertHit "$appname" "$token"
  assertHeader "$appname" /foo.xml "Content-Type" "/xml"
}

test_nanoc() {
  sample_app nanoc
  run_cmd git push puzzle master
  assertHit "$appname" "hello from nanoc"
}

test_jekyll() {
  sample_app jekyll
  run_cmd git push puzzle master
  assertHit "$appname" "hello from jekyll"
}

test_ruby() {
  sample_app rack
  run_cmd git push puzzle master
  assertHit "$appname" "hello from rack"
}

test_python3() {
  sample_app django
  run_cmd git push puzzle master
  assertHit "$appname" "hello from django"
}

. shunit2
