#!/bin/sh

set -e

run() {
  echo "$@"
  "$@"
}

if [ -r ./config.mk ]; then
  . ./config.mk
fi
if [ -z "$PREFIX" ]; then
  PREFIX=/usr/local
fi

for f in $@; do
  run mkdir -p "$DESTDIR/$PREFIX/$(dirname "$f")"
  run cp -a "$f" "$DESTDIR/$PREFIX/$f"
done
