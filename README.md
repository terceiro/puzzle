# puzzle - modular Platform as a Service implementation

`puzzle` is Platform as a Service (PaaS) implementation designed for making
personal use simple and straightforward. The goal is for a Platform using puzzle
to be able to start small with little or no effort, and only when, and if,
necessary, add the necessary complexity required for scaling out.

A simple deployment should be able to use a single server, and a larger one
should be able to use multiple servers; both cases should require as little
manual configuration as possible. The initial version only supports the first
case (a single server); see [ROADMAP.otl](ROADMAP.otl) for details about the
development.

`puzzle` is design so that components can be swapped in and out. For example,
users should be able to choose either `nginx` or `apache2` as a web server
frontend, and even switching from one to the other after already having
applications running.

See the [doc directory](doc/) directory for documentation.

## License

Copyright © 2016-2020 Antonio Terceiro

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
