VERSION = $(shell awk -F = '/^export PUZZLE_VERSION=/ {print($$2)}' bin/puzzlectl)

PREFIX ?= /usr/local

BIN = bin/puzzlectl
MAN = $(patsubst bin/%, man/%.md, $(BIN))

CLIENT_BIN = bin/puzzle

PLUGIN_BIN = $(wildcard plugins/*/bin/*)
PLUGIN_MAN = $(shell ls -1 $(PLUGIN_BIN) | sed -e 's|bin/\(.*\)|man/puzzlectl-\1.md|')

MANPAGES = $(patsubst %.md, %.1, $(MAN) $(PLUGIN_MAN))
CLIENT_MANPAGES = $(patsubst bin/%, %.1, $(CLIENT_BIN))

all: $(MANPAGES) $(CLIENT_MANPAGES)

-include .git/config.mk
-include config.mk

SERVICES = $(patsubst %.in, %, $(wildcard init/systemd/*.service.in))

all: $(SERVICES)

$(SERVICES): % : %.in
	PREFIX=$(PREFIX) $^

SOURCE_DATE_EPOCH ?= $(shell date +%s)
DATE = $(shell LC_ALL=C date -u -d "@$(SOURCE_DATE_EPOCH)" '+%B %Y')

$(MANPAGES): %.1 : %.md
	pandoc --standalone --to man \
		-V title:$(shell basename $*) \
		-V section:1 \
		-V footer:$(shell basename $*) \
		-V header:'User commands' \
		-V date:"$(DATE)" \
		-o $@ $<
	sed -i -e 's/\.SH\s*puzzlectl.-/.SH NAME\npuzzlectl\\-/' $@


$(CLIENT_MANPAGES): %.1: bin/%
	help2man \
		--no-info \
		--output=$@ \
		$<


.PHONY: test
test: all


SCRIPTS =$(wildcard bin/* plugins/*/bin/* plugins/*/hooks/* plugins/*/service/* lib/puzzle/shell)
SHELL_CODE = $(wildcard plugins/*/lib/*.sh lib/puzzle/*.sh) $(shell file -i $(SCRIPTS) | grep text/x-shellscript | cut -d: -f1)

test-shellcheck:
	shellcheck \
	  --exclude SC1090,SC1091 \
	  --format gcc \
	  --shell dash \
	  $(SHELL_CODE)

TEST_FILE ?= test/runall.sh
test-remote:
	util/remote/run-tests $(TEST_FILE)

install: all install-lib install-etc
	install -d -m 0755                $(DESTDIR)$(PREFIX)/bin
	install    -m 0755 $(BIN)         $(DESTDIR)$(PREFIX)/bin
	install -d -m 0755                $(DESTDIR)$(PREFIX)/lib/puzzle/
	install -d -m 0755                $(DESTDIR)$(PREFIX)/share/man/man1/
	install    -m 0644 $(MANPAGES)    $(DESTDIR)$(PREFIX)/share/man/man1/
	install -d -m 0755                $(DESTDIR)/lib/systemd/system/
	install    -m 0644 $(SERVICES)    $(DESTDIR)/lib/systemd/system/

install-client: $(CLIENT_MANPAGES)
	install -d -m 0755                $(DESTDIR)$(PREFIX)/bin
	install    -m 0755 $(CLIENT_BIN)  $(DESTDIR)$(PREFIX)/bin
	sed -i -e 's/^VERSION=.*/VERSION=$(VERSION)/' \
	                                  $(DESTDIR)/$(PREFIX)/bin/puzzle
	install -d -m 0755                $(DESTDIR)$(PREFIX)/share/man/man1/
	install    -m 0644 $(CLIENT_MANPAGES) \
	                                  $(DESTDIR)$(PREFIX)/share/man/man1/

LIB_FILES = $(shell find lib -follow -type f -not -path '*/man/*')
install-lib:
	./install.sh $(LIB_FILES)


.PHONY: install-etc
ETC = $(shell find etc -type f)
install-etc:
	for f in $(ETC); do \
		mkdir -p $(DESTDIR)/$$(dirname $$f); \
		install -m 0644 $$f $(DESTDIR)/$$(dirname $$f); \
	done


version:
	@echo $(VERSION)

MTIME = $(shell LC_ALL=C date --utc --iso-8601=seconds -d "@$(SOURCE_DATE_EPOCH)")

# if not under a git repository, $(HEAD) will be empty, and then MANIFEST will
# not be rebuilt.
HEAD = $(shell ls -1 .git/HEAD 2> /dev/null)

clean:
	$(RM) $(MANPAGES) $(CLIENT_MANPAGES)
	$(RM) $(SERVICES)

distclean: clean
	$(RM) config.mk

.PHONY: todo
todo:
	@grep 'FIXME\|TODO' --recursive --exclude=Makefile *
